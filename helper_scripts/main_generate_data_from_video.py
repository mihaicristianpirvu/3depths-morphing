import yaml
import numpy as np
from typing import Dict
from media_processing_lib.video import MPLVideo, tryReadVideo
from collections import OrderedDict
from functools import partial
from matplotlib.cm import hot

import sys
sys.path.append("/scratch/nvme0n1/ngc/video-representations-extractor")
from video_representations_extractor import VideoRepresentationsExtractor
from representations import Representation, getRepresentation
from representations.dummy_representation import DummyRepresentation

class ScaleDepth(Representation):
	def __init__(self, name, dependencies, dependencyAliases, windowSize:int, invalidPercentage:int):
		super().__init__(name, dependencies, saveResults="resized_only", dependencyAliases=dependencyAliases)
		assert len(dependencies) == 2
		assert "unscaled" in dependencyAliases
		assert "metric" in dependencyAliases
		self.windowSize = windowSize
		self.invalidPercentage = invalidPercentage
		self.scaleFactor = None

	def getParameters(self):
		return {"windowSize":self.windowSize, \
			"invalidPercentage":self.invalidPercentage, "scaleFactor":self.scaleFactor}

	def computeThisScaleMedian(self, unscaled, metric):
		valid = np.where((metric != 0) & (metric != 1))
		unscaled = unscaled[valid]
		if np.prod(unscaled.shape) <= np.prod(metric.shape) * self.invalidPercentage / 100:
			return None
		metric = metric[valid]
		thisScaleFactor = np.median(metric / unscaled)
		return thisScaleFactor

	def make(self, t:int) -> np.ndarray:
		Range = np.clip(list(range(t-self.windowSize+1, t+1)), 0, len(self.video))
		unscaled = np.array([self.unscaled[i]["data"] for i in Range])
		metric = np.array([self.metric[i]["data"] for i in Range])
		thisScaleFactor = self.computeThisScaleMedian(unscaled, metric)
		if thisScaleFactor is None:
			thisScaleFactor = self.scaleFactor

		# Update current scale factor for next iteration
		self.scaleFactor = thisScaleFactor
		data = self.unscaled[t]["data"]
		data = data * self.scaleFactor
		data = np.clip(data, 0, 1)
		return {"data" : data, "extra" : {"scaleFactor" : self.scaleFactor}}

	def makeImage(self, x:Dict) -> np.ndarray:
		y = x["data"]
		assert y.min() >= 0 and y.max() <= 1
		y = hot(y)[..., 0 : 3]
		y = np.uint8(y * 255)
		return y

	def setup(self):
		self.unscaled = self.dependencies[self.dependencyAliases.index("unscaled")]
		self.metric = self.dependencies[self.dependencyAliases.index("metric")]
		if not self.scaleFactor is None:
			return

		while True:
			randomIx = np.random.randint(len(self.video))
			unscaled = self.unscaled[randomIx]["data"]
			metric = self.metric[randomIx]["data"]
			scaleFactor = self.computeThisScaleMedian(unscaled, metric)
			if not scaleFactor is None:
				print("[ScaleDepth::setup] Initial scaling factor: %2.2f" % scaleFactor)
				self.scaleFactor = scaleFactor
				break

class DepethEnsembleMean(Representation):
	def make(self, t:int) -> np.ndarray:
		items = [dep[t]["data"].copy() for dep in self.dependencies]
		res, cnt = items[0] * 0, items[0] * 0

		for x in items:
			x[x == 1] = 0
			res += x
			cnt += (x != 0)
		res = res / cnt
		res[~np.isfinite(res)] = 1
		if res.min() < 0 or res.max() > 1:
			breakpoint()
		return res

	def makeImage(self, x:Dict) -> np.ndarray:
		y = x["data"]
		assert y.min() >= 0 and y.max() <= 1
		y = hot(y)[..., 0 : 3]
		y = np.uint8(y * 255)
		return y

	def setup(self):
		pass

def makeImageFn(x):
	y = x["data"]
	assert y.min() >= 0 and y.max() <= 1
	y = hot(y)[..., 0 : 3]
	y = np.uint8(y * 255)
	return y

def main():
	yamlCfg = yaml.safe_load(open(sys.argv[1], "r"))
	video = tryReadVideo(yamlCfg["videoPath"], vidLib="pims")
	outputResolution = list(map(lambda x : int(x), yamlCfg["outputResolution"].split(",")))
	outputDir = Path(yamlCfg["outputDir"]).absolute()

	representations = yamlCfg["representations"]
	representations["sfm"] = DummyRepresentation(name="sfm", makeImageFn=makeImageFn)
	representations["scaled_unsup_dpt"] = ScaleDepth(name="scaled_unsup_dpt", \
		dependencies=["unsup_dpt", "odoflow_raft"], dependencyAliases=["unscaled", "metric"], \
		windowSize=5, invalidPercentage=20)
	representations["scaled_unsup_jiaw"] = ScaleDepth(name="scaled_unsup_jiaw", \
		dependencies=["unsup_jiaw", "odoflow_raft"], dependencyAliases=["unscaled", "metric"], \
		windowSize=5, invalidPercentage=20)
	representations["depth_ensemble_mean_2"] = DepethEnsembleMean(name="depth_ensemble_mean_2", \
		dependencies=["scaled_unsup_jiaw", "odoflow_raft"])
	representations["depth_ensemble_mean_3"] = DepethEnsembleMean(name="depth_ensemble_mean_3", \
		dependencies=["scaled_unsup_jiaw", "scaled_unsup_dpt", "odoflow_raft"])
	representations["depth_ensemble_mean_4"] = DepethEnsembleMean(name="depth_ensemble_mean_4", \
		dependencies=["scaled_unsup_jiaw", "scaled_unsup_dpt", "odoflow_raft", "odoflow_rife"])
	vre = VideoRepresentationsExtractor(video, outputDir, outputResolution, representations=representations)
	startIx = int(sys.argv[2]) if len(sys.argv) == 3 else 0
	collageOrder = ["rgb", "sfm", "scaled_unsup_jiaw", "scaled_unsup_dpt", "odoflow_rife", "odoflow_raft", \
		"depth_ensemble_mean_2", "depth_ensemble_mean_3", "depth_ensemble_mean_4"]
	vre.doExport(startIx, exportCollage=True, collageOrder=collageOrder)

if __name__ == "__main__":
	main()

import sys
import numpy as np
import pandas as pd
from argparse import ArgumentParser
from pathlib import Path
from natsort import natsorted
from tqdm import trange

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--gtDir", required=True)
    parser.add_argument("--maskDir", required=True)
    parser.add_argument("--predictionDirs", required=True)
    parser.add_argument("--N", type=int)
    args = parser.parse_args()
    args.predictionDirs = [Path(x).absolute() for x in args.predictionDirs.split(",")]
    for x in args.predictionDirs:
        assert x.exists()
    args.gtDir = Path(args.gtDir).absolute()
    args.maskDir = Path(args.maskDir).absolute()
    return args

def fPath(x):
    y = Path(x)
    assert y.exists()
    y = y.glob("*.npz")
    y = [str(x) for x in y]
    y = natsorted(y)
    y = [Path(x).absolute() for x in y]
    y = np.array(y)
    return y

def loadNpz(paths):
    items = []
    for i in trange(len(paths), desc="Loading Npz"):
        path = paths[i]
        item = np.load(path, allow_pickle=True)["arr_0"]
        if item.dtype == np.object:
            item = item.item()
            assert isinstance(item, dict)
            assert "data" in item
            item = item["data"]
        assert item.dtype in (np.uint8, np.float32)
        if item.shape[0] == 1:
            item = item[0]
        item = np.clip(item, 0, 1)
        assert item.min() >= 0 and item.max() <= 1
        items.append(item)
    items = np.array(items)
    return items

def fMask(x):
    whereBad = (x == 0) | (x == 1)
    x = x * 0 + 1
    x[whereBad] = 0
    x = x.astype(np.bool)
    return x

def loss(y, t, mask):
    L1 = np.abs(y - t)
    # Weighted mean instead of .mean() which would count zeros as well.........
    L1 = (L1 * mask).sum() / (np.spacing(1) + mask.sum()) * 400
    return L1

def scaled_loss(y, t, mask):
    L1 = np.abs(y - t)
    L1 = L1 / (t + np.spacing(1))
    L1 = (L1 * mask).sum() / mask.sum() * 100
    return L1

def cnt(mask, total):
    Sum = (~mask).sum()
    return 100 - (Sum / total * 100)

def f(y, t, mask, totalPixels):
    l, sl = loss(y, t, mask), scaled_loss(y, t, mask)
    return l, sl

def getOneResult(y, t, mask):
    # print("[main_metrics_loco] Found: GT=%s. Prediction=%s. Mask=%s" % (t.shape, y.shape, mask.shape))

    totalPixels = np.prod(t.shape)
    cntSfmNan = (t == 0).sum()
    cntMaskNan = (~mask).sum()
    # print("[main_metrics_loco] Total pixels: %d" % totalPixels)
    # print("[main_metrics_loco] Sfm Labels NaNs: %d (%2.2f%%)" % (cntSfmNan, cntSfmNan * 100 / totalPixels))
    # print("[main_metrics_loco] Mask NaNs: %d (%2.2f%%)" % (cntMaskNan, cntMaskNan * 100 / totalPixels))

    maskSfmOnly = t != 0
    # print("[main_metrics_loco] Removing Sfm NaNs only.")
    l1, sl1 = f(y, t, maskSfmOnly, totalPixels)
    cnt1 = cnt(maskSfmOnly, totalPixels)

    maskBoth = maskSfmOnly & mask
    # print("[main_metrics_loco] Removing Both (sfm & flowOdo) <=> good area.")
    l2, sl2 = f(y, t, maskBoth, totalPixels)
    cnt2 = cnt(maskBoth, totalPixels)

    maskBadArea = maskSfmOnly * (~mask)
    # print("[main_metrics_loco] Removing Sfm NaNs and keeping flowOdo NaNs <=> bad area.")
    l3, sl3 = f(y, t, maskBadArea, totalPixels)
    cnt3 = cnt(maskBadArea, totalPixels)

    return l1, sl1, cnt1, l2, sl2, cnt2, l3, sl3, cnt3, len(y), totalPixels

def getResults(t, mask, predictionDirs, perm):
    rows = []
    for yDir in predictionDirs:
        y = fPath(yDir)
        y = loadNpz(y[perm])
        res = getOneResult(y, t, mask)
        row = [yDir.name, *res]
        rows.append(row)
        del y
    df = pd.DataFrame(rows, columns=["Predictions Dir", "L1 (all)", "Scaled-L1 (all)", "Px% (all)", \
        "L1 (good)", "Scaled-L1 (good)", "Px% (good)", "L1 (bad)", "Scaled-L1 (bad)", "Px% (bad)", \
        "Data count", "Total Px"]).sort_values("L1 (good)")
    return df

def main():
    args = getArgs()

    t, mask = fPath(args.gtDir), fPath(args.maskDir)
    np.random.seed(42)
    perm = np.random.permutation(len(t))[0 : args.N] if not args.N is None else np.arange(len(t))
    t, mask = loadNpz(t[perm]), fMask(loadNpz(mask[perm]))
    df = getResults(t, mask, args.predictionDirs, perm)
    print(df)
    savePath = "%s/results.csv" % args.gtDir.parents[0]
    df.to_csv(savePath)

if __name__ == "__main__":
    main()

baseDir=$1
# predDir=(odoflow_raft odoflow_rife unsup_dpt scaled_unsup_dpt unsup_jiaw scaled_unsup_jiaw depth_ensemble_mean_2 depth_ensemble_mean_3 depth_ensemble_mean_4)
predDir=(train_ens2_rgb train_ens2_rgbd train_ens4_rgb train_ens4_rgbd)
predStr=""
for x in ${predDir[@]}; do
	predStr="${predStr},$baseDir/$x"
done
predStr=$(echo $predStr | cut -c2-${#predStr})
gtDir="$1/sfm"
maskDir="$1/odoflow_raft"

#echo $predStr;
python main_metrics_loco.py --gtDir $gtDir --maskDir $maskDir --predictionDirs $predStr


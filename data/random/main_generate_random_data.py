import numpy as np
from pathlib import Path
from tqdm import trange

def generate(baseDir:str, N:int):
	dataset = np.random.random((3, N, 256, 426)).astype(np.float32)
	Dirs = ["unsupervised", "flow_odometry", "sfm"]
	Dirs = ["%s/%s" % (baseDir, dir) for dir in Dirs]
	for Dir, data in zip(Dirs, dataset):
		Path(Dir).mkdir(exist_ok=False, parents=True)
		for i in trange(N, desc=Dir):
			np.savez_compressed("%s/%d.npz" % (Dir, i), data[i])
	Path("%s/rgb" % baseDir).mkdir(exist_ok=False, parents=True)
	rgb = np.random.random((N, 256, 426, 3)).astype(np.float32)
	for i in trange(N, desc="rgb"):
		np.savez_compressed("%s/rgb/%d.npz" % (baseDir, i), rgb[i])

def main():
	generate("random/train", N=80)
	generate("random/validation", N=20)
	print("Done!")

if __name__ == "__main__":
	main()
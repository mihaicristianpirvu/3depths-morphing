import torch as tr
import numpy as np
import torch.optim as optim
from overrides import overrides
from typing import Dict
from functools import partial
from nwmodule import NWModule
from nwmodule.utilities import device
from nwmodule.callbacks import SaveModels, PlotMetrics, SaveHistory, RandomPlotEachEpoch
from nwmodule.schedulers import ReduceLRAndBacktrackOnPlateau
from matplotlib.cm import hot
from media_processing_lib.image import toImage, tryWriteImage

from .safeuavtiny import SafeUAVTiny
from .safeuavlarge import SafeUAVLarge

def l2ValidOnly(y, t):
	whereValid = t > 0
	L2 = (y - t)**2
	L = (whereValid * L2).mean()
	L[L != L] = 0
	return L

def metricSfmMeter(y, t, **kwargs):
	assert "metersFactor" in kwargs
	t = t["sfm"]
	whereValid = t > 0
	L1 = np.abs(y - t)
	L1Valid = L1 * whereValid
	L1ValidMetric = L1Valid * kwargs["metersFactor"]
	L = L1ValidMetric.mean()
	return L

def plotFn(x, y, t, gtKey):
	MB = len(x["rgb"])
	items = []
	items.append(np.array([toImage(x["rgb"][i]) for i in range(MB)]))
	if "sfm" in x:
		items.append(np.array([toImage(hot(x["sfm"][i])) for i in range(MB)]))
	x.pop("rgb")
	gt = x.pop(gtKey)
	items.append(np.array([toImage(hot(gt[i])) for i in range(MB)]))
	for k in x:
		img = np.array([toImage(hot(x[k][i])) for i in range(MB)])
		items.append(img)
	items.append(np.array([toImage(hot(y[i])) for i in range(MB)]))
	stack = np.concatenate(items, axis=2)
	for i in range(MB):
		tryWriteImage(stack[i], "%d.png" % i)

class Model(NWModule):
	def __init__(self, baseModel:NWModule, hyperParameters:Dict):
		super().__init__(hyperParameters=hyperParameters)
		self.baseModel = baseModel

		if hyperParameters["trainAlgorithm"]["type"] == "simpleMean":
			self.algorithm = partial(self.simpleMean, gtKey=hyperParameters["trainAlgorithm"]["gtKey"])
		elif hyperParameters["trainAlgorithm"]["type"] == "simpleMean_rgbd":
			self.algorithm = partial(self.simpleMeanRGBD, gtKey=hyperParameters["trainAlgorithm"]["gtKey"], \
				depthInKey=hyperParameters["trainAlgorithm"]["depthInKey"])
		else:
			assert False, "Unknown algorithm: %s" % hyperParameters["trainAlgorithm"]["type"]

	def simpleMean(self, x, gtKey:str):
		rgb = x["rgb"].transpose(1, 3).transpose(2, 3)
		depthNetwork = self.baseModel(rgb).squeeze()
		L = l2ValidOnly(depthNetwork, x[gtKey])
		return depthNetwork, L

	def simpleMeanRGBD(self, x, gtKey:str, depthInKey:str):
		rgbd = tr.cat([x["rgb"], x[depthInKey].unsqueeze(dim=-1)], dim=-1)
		rgbd = rgbd.transpose(1, 3).transpose(2, 3)
		depthNetwork = self.baseModel(rgbd).squeeze()
		L = l2ValidOnly(depthNetwork, x[gtKey])
		return depthNetwork, L

	@overrides
	def networkAlgorithm(self, trInputs, trLabels, isTraining:bool, isOptimizing:bool):
		depthNetwork, loss = self.algorithm(trInputs)
		if loss != loss:
			breakpoint()
		self.updateOptimizer(loss, isTraining, isOptimizing)
		return depthNetwork, loss
	
	def forward(self, trInputs):
		return self.algorithm(trInputs)[0]

def getModel(modelCfg:Dict, trainCfg:Dict):
	dIn = {
		"simpleMean":3,
		"simpleMean_rgbd":4
	}[trainCfg["trainAlgorithm"]["type"]]

	if modelCfg["type"] == "SafeUAVTiny":
		assert "numFilters" in modelCfg["parameters"]
		baseModel = SafeUAVTiny(dIn=dIn, dOut=1, **modelCfg["parameters"])
	elif modelCfg["type"] == "SafeUAVLarge":
		assert "numFilters" in modelCfg["parameters"]
		baseModel = SafeUAVLarge(dIn=dIn, dOut=1, **modelCfg["parameters"])
	else:
		assert False, "Unknown model: %s" % (modelCfg["type"])

	assert trainCfg["optimizerType"] == "Adam"
	# Set to "alternate", "simpleMean", "sfmSupervised" etc.
	modelCfg["trainAlgorithm"] = trainCfg["trainAlgorithm"]
	hasSfm = "sfm" in trainCfg["trainAlgorithm"]["dims"]
	model = Model(baseModel, hyperParameters=modelCfg).to(device)
	model.setOptimizer(optim.Adam, lr=trainCfg["learningRate"])
	scheduler = ReduceLRAndBacktrackOnPlateau(model, "Loss", trainCfg["patience"], trainCfg["factor"])
	model.setOptimizerScheduler(scheduler)
	model.addCallbacks([
		SaveModels("best", "Loss"),
		SaveModels("last", "Loss"),
		SaveHistory("history.txt"), \
		RandomPlotEachEpoch(partial(plotFn, gtKey=trainCfg["trainAlgorithm"]["gtKey"]))
	])
	if hasSfm:
		model.addMetric("LossWrtSfm", partial(metricSfmMeter, metersFactor=400))
		model.addCallback(PlotMetrics(["Loss", "LossWrtSfm"]))
		model.addCallback(SaveModels("best", "LossWrtSfm"))
	print(model.summary())
	return model